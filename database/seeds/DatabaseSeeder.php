<?php

use App\Answer;
use App\Question;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

       /* factory(\App\User::class, 5)->create()->each(function($user){
            //this body will run for every user that has been created
            //make() will create object of Question class by using factory
            //create() ko array chahiye hota hai as argument so make() is returning object of Question 
            for($i = 1; $i<=rand(5,10); $i++){
                $user->questions()->create(factory(\App\Question::class)->make()->toArray());
            }
        }); */
        factory(\App\User::class, 5)->create()->each(function($user){
            //isme abhi vo har ek question pe answers bannn rhe haiii!
            $user->questions()
            ->saveMany(factory(Question::class,rand(2,5))->make())
            ->each(function($question){
                $question->answers()->saveMany(factory(Answer::class,rand(2,7))->make());
            });
        });

        
    }
}
